#!/usr/bin/env ruby
require 'rack'
require 'rack/handler/puma'
require 'json'

app = Proc.new do |env|
  payload = {
    'Process.pid' => Process.pid,
    'Thread.current' => Thread.current,
    'Time.now' => Time.now,
    'env' => env
  }.to_json
  ['200', {'Content-Type' => 'application/json'}, [payload]]
end

Rack::Handler::Puma.run(app, {Verbose: true, Port: 3000, Threads: '3:3'})
