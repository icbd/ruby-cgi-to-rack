#!/usr/bin/env ruby

require "cgi"
require 'json'

cgi = CGI.new
info = {
  'Process.pid' => Process.pid,
  'Thread.current' => Thread.current,
  'Time.now' => Time.now,
}

puts "Content-Type:application/json\n\n"
puts info.merge(cgi.params).to_json
