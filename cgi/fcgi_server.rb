#!/usr/bin/env ruby

require "fcgi"
require 'json'

FCGI.each_cgi do |cgi|
  info = {
    'Process.pid' => Process.pid,
    'Thread.current' => Thread.current,
    'Time.now' => Time.now,
  }
  puts "Content-Type:application/json\n\n"
  puts info.merge(cgi.params).to_json
end

=begin
brew install fcgi spawn-fcgi
chmod cgi/fcgi_server.rb
spawn-fcgi -u _www -U _www  -f /Users/cbd/Coding/ruby/archeology/cgi/fcgi_server.rb -p 3000
=end

